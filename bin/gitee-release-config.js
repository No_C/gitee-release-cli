#!/usr/bin/env node

const program = require('commander');
const { config } = require('../src/index');
const logger = require('../src/logger');

program
  .usage('<name> <value>')
  .action((name, value) => {
    if (typeof value !== 'string') {
      logger.log(config.get(name) || '');
    } else {
      config.set(name, value || '');
    }
  })
  .parse(process.argv);
